package callstack;

/**
 * Author: Jan de Rijke
 */
public class CallstackDemo {
	public static void main(String[] args) {


		Product lotr = new Book("TT", "A 20th century classic.",
			19.90, "J.R.R. Tolkien",
			"The Two Towers");
		System.out.println("Book: " + lotr);
	}

}

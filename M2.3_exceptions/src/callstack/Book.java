package callstack;

/**
 * Author: Jan de Rijke
 */
public class Book extends Product {
	private final String auhtor;
	private final String title;

	public Book(String code, String description, double price, String auhtor, String title) {
		super(code, description, price);
		this.auhtor = auhtor;
		this.title = title;
	}

	@Override
	public String toString() {
		return "Book{" +
			"auhtor='" + auhtor + '\'' +
			", title='" + title + '\'' +
			"} " + super.toString();
	}
}

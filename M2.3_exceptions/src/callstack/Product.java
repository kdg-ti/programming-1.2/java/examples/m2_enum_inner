package callstack;

import java.util.StringJoiner;

/**
 * Author: Jan de Rijke
 */
public class Product {
	protected final String code;
	protected final String description;
	protected final double price;

	public Product(
		String code, String description, double price) {
		if (code.length() < 3){
			// we should really check if code is not null,
			// now this will throw a NullPointerException
			throw new IllegalArgumentException("Code must be at least 3 characters");
		}
		this.code = code;
		this.description = description;
		this.price = price;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
			.add("code='" + code + "'")
			.add("description='" + description + "'")
			.add("price=" + price)
			.toString();
	}
}

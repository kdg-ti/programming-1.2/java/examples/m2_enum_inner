package catching;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CatchChainDemo {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter 2 numbers for which you want to calculate the remainder of the division: ");
    try {
      int number1 = scanner.nextInt();
      int number2 = scanner.nextInt();
      int remainder =  number1 % number2;
      System.out.printf("%d %% %d = %d",
          number1, number2, remainder);
    } catch (InputMismatchException ex) {
      System.out.println("All numbers must be integers!");
    } catch (ArithmeticException ex) {
      System.out.println("Cannot divide by zero");
    }
  }
}

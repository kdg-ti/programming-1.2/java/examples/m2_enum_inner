package bank;

public class BankAccount {
  public class Safe {
  private int number;

  public Safe(int number) {
    this.number = number;
  }

  public int getNumber() {
    return number;
  }

  @Override
  public String toString() {
    return "Safe number " + number;
  }
}

  private String iban;
  private double balance;
  private Safe safe;


  public Safe getSafe() {
  return safe;
}

  public BankAccount(String iban, double balance,
                     int nummer) {
    this.iban = iban;
    this.balance = balance;
    safe = new Safe(nummer);
  }

  public int getNumber() {
    return safe.number;
  }

  @Override
  public String toString() {
    return "IBAN: " + iban + " Balance: " +
        balance + " Number: " + safe.number;
  }
}

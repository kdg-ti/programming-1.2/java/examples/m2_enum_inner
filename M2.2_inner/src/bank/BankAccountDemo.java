package bank;

public class BankAccountDemo {
  public static void main(String[] args) {
    BankAccount account1 = new BankAccount("BE17 0000 0000 2121",
        1000, 142857);
    System.out.println(account1);
    System.out.println(account1.getNumber());
    System.out.println(account1.getSafe());

    BankAccount.Safe safe = account1.new Safe(4662);
    System.out.println(safe);
    System.out.println(safe.getNumber());
  }

}

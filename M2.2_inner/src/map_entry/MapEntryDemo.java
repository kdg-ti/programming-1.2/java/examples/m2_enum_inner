package map_entry;

import java.util.Map;

public class MapEntryDemo {
  public static void main(String[] args) {
    Map<String, String> messages = Map.ofEntries(
        Map.entry("en", "Hello!"),
        Map.entry("es", "¡Hola!"),
        Map.entry("de", "Hallo!")
    );

    for (Map.Entry<String, String> entry : messages.entrySet()) {
      System.out.printf("%s --> %s%n",
          entry.getKey(),
          entry.getValue());
    }
  }
}

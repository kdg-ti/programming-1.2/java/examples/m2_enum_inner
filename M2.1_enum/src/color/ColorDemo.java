package color;

public class ColorDemo {
  public static void main(String[] args) {
    Color foreground = Color.BLUE;
    Color background = Color.WHITE;
    printColor(foreground);
    printColor(background);  
    printColor2(background);
    printAllColorEnumValues();
  }

  private static void printColor(Color color) {
    System.out.println("Name:" + color.name());
    System.out.println("Ordinal: " + color.ordinal());
  }

  private static void printColor2(Color color){
    if (color == Color.BLACK){
      System.out.println("Black is beautiful!");
    }

    System.out.println( switch(color){
      case BLACK -> "Black";
      case WHITE ->"White";
      case RED -> "Red";
      case GREEN -> "Green";
      case BLUE -> "Blue";
      case YELLOW -> "Yellow";
    });
  }

  private static void printAllColorEnumValues() {
    for(Color color : Color.values()){
      System.out.printf("%nName: %s (%d)",
          color.toString(),
          color.ordinal());
    }
  }


}

package nested;

import color_extend.Color;

public class Car {
  public enum Paint {
    SOLID, METALLIC, PEARL, MATTE;
  }

  private String brand;
  private Color color;
  private Paint paint;

  public Car(String brand, Color color,Paint paint) {
    this.brand = brand;
    this.color = color;
    this.paint = paint;
  }

  @Override
  public String toString() {
    return "Car{" +
      "brand='" + brand + '\'' +
      ", color=" + color +
      ", paint=" + paint +
      '}';
  }
}

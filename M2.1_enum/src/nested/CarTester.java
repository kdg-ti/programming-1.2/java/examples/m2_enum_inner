package nested;

import color_extend.Color;

public class CarTester {
  public static void main(String[] args) {
    Car myDreamCar = new Car("Ferrari",
      Color.RED,
      Car.Paint.METALLIC);
    System.out.println(myDreamCar);
  }
}

package color_extend;

public enum Color {
    BLACK(0x000000), WHITE(0xFFFFFF),
    RED(0xFF0000), GREEN(0x00FF00),
    BLUE(0x0000FF), YELLOW(0xFFFF00);

    private final int rgb;

    Color(int rgb){
        this.rgb = rgb;
    }

    public int getRgb() {
        return rgb;
    }

    @Override
    public String toString() {
        return String.format("%s (%x)", name(), rgb);
    }
}
